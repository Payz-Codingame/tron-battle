class Position:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.owner = None

    def get_distance(self,player):
        other = player.positions[-1]
        return abs(self.x - other.x) + abs(self.y - other.y)

    def set_owner(self,player_list):
        for player in player_list.players:
            dist = self.get_distance(player)
            if (self.owner == None) or (dist < self.get_distance(self.owner)):
                self.owner = player

class Player:
    def __init__(self):
        self.positions = []
    
    def __eq__(self,other):
        if self is None or other is None: return False
        return self.positions == other.positions

    def __ne__(self,other):
        return self.positions != other.positions

    def set_initial_position(self,string):
        x,y,_,_ = [int(i) for i in string.split()]
        self.positions = [Position(x,y)]

    def add_position(self,string):
        _,_,x,y = [int(i) for i in string.split()]
        self.positions.append(Position(x,y))

    def get_amount_tiles(self,positions):
        owned_tiles = [tile for tile in positions if tile.owner == self]
        return len(owned_tiles)

class PlayerList:
    def __init__(self,string=""):
        if string == "":
            self.players = [Player()]
        else:
            num_players,player_id = [int(i) for i in string.split()]
            self.players = []
            for i in range(num_players):
                self.players.append(Player())
                self.players[i].is_player = (i == player_id)
    
    def get_amount(self):
        return len(self.players)

    def get_player(self):
        player = [player for player in self.players if player.is_player]
        return player[0]
