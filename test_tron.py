# Testfile for Tron
from tron import *

def test_input_num_players():
    test_string = "2 0"
    player_list = PlayerList(test_string)
    assert(player_list.get_amount() == 2)

def test_input_init_pos():
    test_string = "10 19 9 19"
    test_id = 0
    player_list = PlayerList()
    player_list.players[test_id].set_initial_position(test_string)
    assert(player_list.players[test_id].positions[0].x == 10)
    assert(player_list.players[test_id].positions[0].y == 19)

def test_player_id():
    test_string = "2 0"
    player_list = PlayerList(test_string)
    assert(player_list.get_player() == player_list.players[0])

def test_manhattan_distance():
    position = Position(5, 8)
    player = Player()
    player.set_initial_position("8 9 8 9")
    distance = position.get_distance(player)
    assert(distance == 4)

def test_add_position():
    player = Player()
    player.set_initial_position("8 9 8 9")
    player.add_position("8 9 8 10")
    assert(player.positions[1].x == 8)
    assert(player.positions[1].y == 10)

def test_position_set_owner():
    player_list = PlayerList("2 0")
    player_list.players[0].set_initial_position("8 9 8 9")
    player_list.players[1].set_initial_position("3 3 3 3")
    position = Position(3,4)
    position.set_owner(player_list)
    assert(position.owner == player_list.players[1])

def test_position_amount_owned_tiles():
    positions = [Position(x,y) for x in range(0,4) for y in range(0,3)]
    player_list = PlayerList("2 0")
    player_list.players[0].set_initial_position("0 0 0 0")
    player_list.players[1].set_initial_position("3 2 3 2")
    [position.set_owner(player_list) for position in positions]
    p1_tiles = player_list.players[0].get_amount_tiles(positions)
    p2_tiles = player_list.players[1].get_amount_tiles(positions)
    assert(p1_tiles == p2_tiles)
